const screen = document.getElementById('calc-screen');
const calcNumBtns = document.getElementsByClassName('calc-btn-num');
let newNum = true;
let decUsed = false;
let num1 = null;
let num2;
let operator;

screen.value = '0';
console.log(calcNumBtns);

document.getElementById("btn-zero").addEventListener("click", function(){
        if (newNum == true){
                screen.value = '0';
                return;
        }
        else{
                screen.value += '0';
        }
});

document.getElementById("btn-one").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '1';
            newNum = false;
            return;
    }
    else{
            screen.value += '1';
    }
});

document.getElementById("btn-two").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '2';
            newNum = false;
            return;
    }
    else{
            screen.value += '2';
    }
});

document.getElementById("btn-three").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '3';
            newNum = false;
            return;
    }
    else{
            screen.value += '3';
    }
});

document.getElementById("btn-four").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '4';
            newNum = false;
            return;
    }
    else{
            screen.value += '4';
    }
});

document.getElementById("btn-five").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '5';
            newNum = false;
            return;
    }
    else{
            screen.value += '5';
    }
});

document.getElementById("btn-six").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '6';
            newNum = false;
            return;
    }
    else{
            screen.value += '6';
    }
});

document.getElementById("btn-seven").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '7';
            newNum = false;
            return;
    }
    else{
            screen.value += '7';
    }
});

document.getElementById("btn-eight").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '8';
            newNum = false;
            return;
    }
    else{
            screen.value += '8';
    }
});

document.getElementById("btn-nine").addEventListener("click", function(){
    if (newNum == true){
            screen.value = '9';
            newNum = false;
            return;
    }
    else{
            screen.value += '9';
    }
});

document.getElementById("btn-decimal").addEventListener("click", function(){
    if (decUsed == false){
            screen.value += '.';
            decUsed = true;
            newNum = false;
    }
});

document.getElementById("btn-add").addEventListener("click", function(){
    if (num1 == null){
            num1 = Number(screen.value);
            
    }
    else{
            num1 += Number(screen.value);
    }
    console.log(num1);
    operator = '+';
    newNum = true;
    decUsed = false;
});

document.getElementById("btn-subtract").addEventListener("click", function(){
    if (num1 == null){
            num1 = Number(screen.value);
            
    }
    else{
            num1 += Number(screen.value);
    }
    console.log(num1);
    operator = '-';
    newNum = true;
    decUsed = false;
});

document.getElementById("btn-divide").addEventListener("click", function(){
    if (num1 == null){
            num1 = Number(screen.value);
            
    }
    else{
            num1 += Number(screen.value);
    }
    console.log(num1);
    operator = '/';
    newNum = true;
    decUsed = false;
});

document.getElementById("btn-multiply").addEventListener("click", function(){
    if (num1 == null){
            num1 = Number(screen.value);
            
    }
    else{
            num1 += Number(screen.value);
    }
    console.log(num1);
    operator = '*';
    newNum = true;
    decUsed = false;
});

document.getElementById("btn-clear").addEventListener("click", function(){
    screen.value = '0';
    num1 = null;
    num2 = null;
    newNum = true;
    decUsed = false;
});

document.getElementById("btn-equals").addEventListener("click", function(){
    num2 = Number(screen.value);
    switch(operator){
            case '+':
                screen.value = num1 + num2;
                break;
            case '-':
                screen.value = num1 - num2;
                break;
            case '*':
                screen.value = num1 * num2;
                break;
            case '/':
                if (num2 == 0){
                    screen.value = "No dividing by 0. Try again!";
                    break;
                }
                screen.value = num1 / num2;
                break;
    }
    num1 = null;
    newNum = true;
    decUsed = false;
});

