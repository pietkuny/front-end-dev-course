

let colorList = ['blue', 'pink', 'red', 'green', 'purple', 'yellow', 'navy', 'brown', 'gray', 'black'];
let result = 0;
let randomNum1;
let randomNum2;
let color1;
let color2;
let n = 1;
function changeColor(target) {
    if (color1 == color2 && target.value == 'yes') {
        result++;
    } else if
    (color1 !== color2 && target.value == 'no') {
        result++;
    } else {
        result--;
    }


    if (result % 10 == 0 && result !== 0) {
        n = getLevel(result);
        document.querySelector('.message').innerText = 'Brawo! Zdobyłeś ' + n + ' poziom!';
        if (n == -1)
            document.querySelector('.message').innerText += '\n\nJesteś przegranym!';
        if (n == 1)
            document.querySelector('.message').innerText += '\n\nGratulacje!!! Jesteś mistrzem w tej grze!!!';
    }
    
    document.querySelector('.result').innerText = result;
    setColor();
}

function setColor() {
    randomNum1 = Math.floor(Math.random() * colorList.length);
    randomNum2 = Math.floor(Math.random() * colorList.length);
    color1 = colorList[randomNum1];
    color2 = colorList[randomNum2];
    document.querySelector('.color').innerText = color1;
    document.querySelector('.color').style.color = color2;
}

function getLevel(v) {
    let i = 1;
    if (v < 0) i = '-1';
    if (v == 10) i = 1;
    if (v == 20) i = 2;
    if (v == 30) i = 3;
    if (v == 40) i = 4;
    if (v == 50) i = 5;
    if (v == 60) i = 6;
    if (v == 70) i = 7;
    if (v == 80) i = 8;
    if (v == 90) i = 9;
    if (v == 100) i = 10;
    return i;

}

setColor();