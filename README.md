# Front-end Dev Course

## My Portfolio Projects

I'm on the begining of the Front-end Developer path, so those projects are my first seriouse projects.
Some of them are really nice for my opinion, but some of them are very basic and have been created just to practice my coding.

Existing projects have been done in #HTML, #CSS, #JavaScript

For future projects i will try to use the same languages but also if possible some new one.

### License

Those projects are open source and have been made in Visual Studio Code Version: 1.74.3

For debugging i used Web browser: Google Chrome Version 109.0.5414.75 but i am sure if anyone who will try to use diffrent browser then still will work.

### General questions and problems!

In case of any of them do not hesitate to contact me.

I hope all of existing projects and some other which are upcoming in near future are not to bad.

